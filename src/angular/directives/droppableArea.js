angular.module('CJApp.directives').directive('droppableArea', function(){
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            elem.bind('drop', function (e, ui) {
                e.preventDefault();
                var origin = event.dataTransfer.getData("text");
                var dest = attr['id'];
                scope.mover(origin, dest);
            });
            elem.bind('dragover', function (e) {
                e.preventDefault();
            });
        }
    };
});
