
angular.module('CJApp.directives').directive('materialSelect',['$timeout', function($timeout){

	var directive = {
		link: link,
		restrict: 'E',
		require: '?ngModel'
	};

	function link(scope, element, attrs, ngModel) {
		if (ngModel) {
			ngModel.$render = create;
		}else {
			$timeout(create);
		}

		function create() {
			element.material_select();
		}
	}

	return directive;
}]);
