
angular.module('CJApp.controllers').controller('registerController',['UserService', '$location', '$rootScope', 'FlashService','$scope', function(UserService, $location, $rootScope, FlashService, $scope){

	console.log('registerController init');

    $scope.registerUser = function() {
		console.log('Registering user', $scope.user);
        $scope.dataLoading = true;
        UserService.Create($scope.user).then(function (response) {
            if (response.success) {
                FlashService.Success('Registration successful', true);
                $location.path('/login');
            } else {
                FlashService.Error(response.message);
                $scope.dataLoading = false;
            }
        });
    }
}]);
