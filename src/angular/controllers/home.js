
angular.module('CJApp.controllers').controller('homeController',['UserService','ReportService', '$rootScope', '$scope', function(UserService,ReportService, $rootScope, $scope){

	console.log('homeController init');
	$rootScope.caseSelected;
	$scope.user = null;
	$scope.Allreport = null;
	loadCurrentUser();
	loadAllReports()

	function loadCurrentUser() {
		UserService.GetByUsername($rootScope.globals.currentUser.username)
		.then(function (user) {
			$scope.user = user;
			console.log($scope.user)
		});
	}

	function loadAllReports() {
		ReportService.GetAll()
		.then(function (reports) {
			$scope.Allreport = reports;
			console.log($scope.Allreport)
		});
	}
	$scope.helpInvitation = function (item) {
		$rootScope.caseSelected = item;
	}

}]);
