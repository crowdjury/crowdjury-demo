angular.module('CJApp.controllers').controller('veredictController',['$location', '$rootScope','$scope', function($location, $rootScope, $scope){

    console.log('veredictController init');
    $rootScope.personRigth;
    $rootScope.personWrong;

    $scope.elegirPerson = function (rigth, wrong){
        $rootScope.personWrong = wrong;
        $rootScope.personRigth = rigth;
    };
}]);
