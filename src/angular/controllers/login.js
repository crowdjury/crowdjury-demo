angular.module('CJApp.controllers').controller('loginController',['$location', 'AuthenticationService', 'FlashService', '$scope', function($location, AuthenticationService, FlashService, $scope){

	console.log('loginController init');

	AuthenticationService.ClearCredentials();

	$scope.login = function() {
		$scope.dataLoading = true;
		AuthenticationService.Login($scope.username, $scope.password, function (response) {
			if (response.success) {
				AuthenticationService.SetCredentials($scope.username, $scope.password);
				$location.path('/');
			} else {
				FlashService.Error(response.message);
				$scope.dataLoading = false;
			}
		});
	};
}]);
