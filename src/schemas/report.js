module.exports = function(db,mongoose) {

	var report = new mongoose.Schema({
		title: String,
		description: String,
		type: String,
		files: [Schema.Types.ObjectId],
		reporter: Schema.Types.ObjectId
	});
	report.methods.edit = function(_title, _description, _type){
		this.title = _title;
		this.description = _description;
		this.type = _type;
	};
	report.methods.addFile = function(_fileID){
		this.files.push(_fileID);
	};
	report.methods.removeFile = function(_fileID){
		for (var i = 0; i < files.length; i++) {
			if (files[i] == _fileID)
				this.files.splice(i,1);
		}
	};
	db.reports = db.model('reports', report);

};
