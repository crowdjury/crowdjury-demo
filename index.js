//Dependencies
var express = require('express');
var mongo = require('mongodb');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var grunt = require('grunt');
var cors = require('cors');

//Configuration
var config = new require('./config');

//Get arguments
var args = process.argv.slice(2);

//Connect
mongoose.connect(config.DBUri);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

//Once connected do actions
db.once('open', function callback () {
    console.log('Connected to DB: '+config.DBUri);
});

//Launch express
var app = express();

//Config Express
app.set('port', (config.port));
app.set('views', __dirname + '/src/');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use('/', express.static(__dirname + '/src/'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cors());

//Add routes
require('./routes')(app, db).addRoutes();

//Start the server
if (args.indexOf('-build') < 0){
	app.listen(app.get('port'), function() {
	    console.log('Crowdjury demo started at port '+app.get('port'));
	});
}

if (args.indexOf('-dev') < 0){
    grunt.tasks(['clean']);
    grunt.tasks(['ngAnnotate']);
	grunt.tasks(['copy:build']);
    //grunt.tasks(['uglify']);
    //grunt.tasks(['cssmin']);
} else {
    grunt.tasks(['clean']);
    grunt.tasks(['copy:dev']);
    grunt.tasks(['watch:dev']);
}
