# Crowdjury Demo

Demo web app for crowdjury platform.

## Installation

`npm install`
`bower install`

## Build

Run the command `npm run build`.

## Develop

Run the command `npm run dev`.
