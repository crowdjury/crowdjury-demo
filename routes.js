
module.exports = function(app, db){

    module.addRoutes = function(){

        app.get('/', module.index);
        app.get('/templates/:name',module.templates);
        app.get('/directives/:name',module.directives);

        app.get('/home', module.index);
        app.get('/login', module.index);
        app.get('/register', module.index);
        app.get('/report', module.index);
        app.get('/buildReport', module.index);
        app.get('/dragAndDrop', module.index);
        app.get('/adward', module.index);
        app.get('/reportOk', module.index);

    }

    module.index = function(req,res){
        res.render('index.html');
    };

    module.templates = function(req,res){
        res.render('templates/' + req.params.name);
    };

    module.directives = function(req,res){
        res.render('directives/' + req.params.name);
    };

    return module;

}
