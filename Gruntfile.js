
module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-ng-annotate');

    grunt.initConfig({

        jshint: {
            options: {
            reporter: require('jshint-stylish')
            },
            build: ['Gruntfile.js', 'src/angular/**/*.js']
        },

        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            min: {
                files: [{
                    expand: true,
                    cwd: 'src/angular/',
                    src: '**/*.js',
                    dest: 'build/angular'
                },{
                    expand: true,
                    cwd: 'src/',
                    src: '**/*.js',
                    dest: 'build/'
                }]
            }
        },

        ngAnnotate: {
            build: {
                files: [{
                    expand: true,
                    src: 'src/angular/**/*.js'
                }]
            }
        },

        copy: {
            dev: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['**'],
                    dest: 'build',
                    options: {
                        mode: '0777'
                    }
                }],
            },
			build: {
                files: [{
                    expand: true,
                    cwd: 'src/angular/',
                    src: ['**'],
                    dest: 'build/angular',
                    options: {
                        mode: '0777'
                    }
                },{
                    expand: true,
                    src: 'src/lenguages.js',
                    dest: 'build/',
                    options: {
                        mode: '0777'
                    }
                }, {
                    expand: true,
                    cwd: 'src/css/',
                    src: ['**'],
                    dest: 'build/css',
                    options: {
                        mode: '0777'
                    }
                }],
            },
        },

        cssmin: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            min: {
                files: [{
                    expand: true,
                    cwd: 'src/css',
                    src: '*.css',
                    dest: 'build/css'
                }]
            }
        },

        watch: {
            dev: {
                files: ['src/**/*.js','src/**/*.css','views/**/*.html'],
                tasks: ['copy',]
            },
            prod: {
                files: ['src/**/*.js','src/**/*.css','views/**/*.html'],
                tasks: ['ngAnnotate, uglify, cssmin',]
            },
            options: {
                nospawn: true,
                livereload: true
            }
        },

        clean: {
            build: {
                src: ["build/angular/**/**.js", "build/css/**.css"]
            }
        }

  });

};
