
var CJApp = angular.module('CJApp', ['CJApp.controllers','CJApp.directives','CJApp.services', 'ngRoute', 'ngCookies', 'LocalStorageModule']);

//Controllers mudule
angular.module('CJApp.controllers', []);
//Services Module
angular.module('CJApp.services', ['LocalStorageModule']);
//Directives Module
angular.module('CJApp.directives', []);

//App Configuraton
CJApp.config(['localStorageServiceProvider', '$routeProvider', '$locationProvider', function (localStorageServiceProvider, $routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {
		controller: 'homeController',
		templateUrl: 'templates/home.html'
	})
	.when('/login', {
		controller: 'loginController',
		templateUrl: 'templates/login.html'
	})
	.when('/report', {
		controller: 'reportController',
		templateUrl: 'templates/report.html'
	})
	.when('/register', {
		controller: 'registerController',
		templateUrl: 'templates/register.html'
	})
	.when('/buildReport', {
        controller: 'buildReportController',
        templateUrl: 'templates/buildReport.html'
    })
    .when('/dragAnDrop', {
        controller: 'dragAndDropController',
        templateUrl: 'templates/dragAndDrop.html'
    })
    .when('/reward', {
        controller: 'rewardController',
        templateUrl: 'templates/reward.html'
    })
    .when('/reportOK', {
        controller: 'reportOkController',
        templateUrl: 'templates/reportOk.html'
    })
    .when('/invitation', {
        controller: 'invitationController',
        templateUrl: 'templates/invitation.html'
    })
    .when('/evidence', {
        controller: 'evidenceController',
        templateUrl: 'templates/evidence.html'
    })
    .when('/evidenceContent', {
        controller: 'evidenceContentController',
        templateUrl: 'templates/evidenceContent.html'
    })
    .when('/veredict', {
        controller: 'veredictController',
        templateUrl: 'templates/veredict.html'
    })
    .when('/confirmVeredict', {
        controller: 'veredictConfirmController',
        templateUrl: 'templates/confirmVeredict.html'
    })
    .when('/recompense', {
        controller: 'veredictConfirmController',
        templateUrl: 'templates/recompense.html'
    })
        .when('/veredictOK', {
        controller: 'veredictOKController',
        templateUrl: 'templates/veredictOK.html'
    })
    .otherwise({ redirectTo: '/login' });

    localStorageServiceProvider
    .setPrefix('crowdjury')
    .setStorageType('localStorage')
    .setNotify(true, true);
}]);

//App run
CJApp.run(['$rootScope', '$location', '$cookieStore', '$http', function ($rootScope, $location, $cookieStore, $http) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn) {
            $location.path('/login');
        }
    });
}]);
