angular.module('CJApp.services').factory('storageService', ['localStorageService','$http','$rootScope', function (localStorageService,$http,$rootScope) {

	var factory = {};

    factory.addItem = function(key, value){
        return localStorageService.set(key, value);
    }
    factory.getItem = function(key, value){
        return localStorageService.get(key);
    }
    factory.removeItem = function(key){
        localStorageService.remove(key);
    }

    return factory;
}]);
