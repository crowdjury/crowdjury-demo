
angular.module('CJApp.services').service('ReportService',['$timeout', '$filter', '$q', function($timeout, $filter, $q){

    var service = {};

    service.GetAll = GetAll;
    service.GetById = GetById;
    service.GetByReportname = GetByReportname;
    service.Create = Create;
    service.Update = Update;
    service.Delete = Delete;

    return service;

    function GetAll() {
        var deferred = $q.defer();
        deferred.resolve(getReports());
        return deferred.promise;
    }

    function GetById(id) {
        var deferred = $q.defer();
        var filtered = $filter('filter')(getReports(), { id: id });
        var report = filtered.length ? filtered[0] : null;
        deferred.resolve(report);
        return deferred.promise;
    }

    function GetByReportname(reportname) {
        var deferred = $q.defer();
        var filtered = $filter('filter')(getReports(), { reportname: reportname });
        var report = filtered.length ? filtered[0] : null;
        deferred.resolve(report);
        return deferred.promise;
    }

    function Create(report) {
        var deferred = $q.defer();

        // simulate api call with $timeout
        $timeout(function () {
            GetByReportname(report.title)
            .then(function (duplicateReport) {
                if (duplicateReport !== null) {
                    deferred.resolve({ success: false, message: 'Reportname "' + report.title + '" is already taken' });
                } else {
                    var reports = getReports();

                        // assign id
                        var lastReport = reports[reports.length - 1] || { id: 0 };
                        report.id = lastReport.id + 1;

                        // save to local storage
                        reports.push(report);
                        setReports(reports);

                        deferred.resolve({ success: true });
                    }
                });
        }, 1000);

        return deferred.promise;
    }

    function Update(report) {
        var deferred = $q.defer();

        var reports = getReports();
        for (var i = 0; i < reports.length; i++) {
            if (reports[i].id === report.id) {
                reports[i] = report;
                break;
            }
        }
        setReports(reports);
        deferred.resolve();

        return deferred.promise;
    }

    function Delete(id) {
        var deferred = $q.defer();

        var reports = getReports();
        for (var i = 0; i < reports.length; i++) {
            var report = reports[i];
            if (report.id === id) {
                reports.splice(i, 1);
                break;
            }
        }
        setReports(reports);
        deferred.resolve();

        return deferred.promise;
    }

    // private functions

    function getReports() {
        if(!localStorage.reports){
            localStorage.reports = JSON.stringify([]);
        }

        return JSON.parse(localStorage.reports);
    }

    function setReports(reports) {
        localStorage.reports = JSON.stringify(reports);
    }
}]);
