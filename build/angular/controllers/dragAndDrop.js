angular.module('CJApp.controllers').controller('dragAndDropController',['UserService', '$rootScope', '$scope', function(UserService, $rootScope, $scope, Upload){

	console.log('dragAndDropController init');
	$scope.file;

	function readURL(input) {
	    for (var i = 0; i < input.files.length; i++) {
		    if (input.files && input.files[i]) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $scope.file = [{
		                base64:e.target.result,
		                name : "name of document"
		             }];
		             $rootScope.reportData.file = $scope.file;
		        }
		    }
		}
	}
	$("#file").change(function(){
	    readURL(this);
	});

}]);
