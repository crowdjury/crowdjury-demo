angular.module('CJApp.controllers').controller('rewardController',['UserService', 'ReportService', 'FlashService', '$location', '$rootScope', '$scope', function(UserService, ReportService, FlashService, $location, $rootScope, $scope){

	console.log("rewardController init");
	var self = this;
	$scope.user = null;
	loadCurrentUser();
	function loadCurrentUser() {
		UserService.GetByUsername($rootScope.globals.currentUser.username).then(function (user) {
			$scope.user = user;
			console.log($scope.user)
		});
	}

   $scope.dataReport = $rootScope.reportData;

	$scope.submitReport = function() {
		console.log("Submiting report on adward");
		var fullName = $scope.user.firstName + " " + $scope.user.lastName;
		$scope.dataReport.fullName = fullName;
		console.log($scope.dataReport);
		$scope.createReport();
	};

	$scope.createReport = function() {
	    console.log('Creating Report', $scope.dataReport);
	    $scope.dataLoading = true;
	    ReportService.Create($scope.dataReport)
	    .then(function (response) {
	        if (response.success) {
	            FlashService.Success('Report successful', true);
	            $location.path('/reportOK');
	            $rootScope.correct = true;
	        } else {
	            FlashService.Error(response.message);
	            $scope.dataLoading = false;
	            $rootScope.error = true;
	        }
	    });
	}

}]);
