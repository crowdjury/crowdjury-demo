angular.module('CJApp.controllers').controller('buildReportController',['$location', '$rootScope', '$scope', function($location, $rootScope, $scope){

	console.log("buildReportController init");

	var self = this;

    $scope.reportData = $rootScope.reportData;
    $(document).ready(function() {
        $('input#input_text, textarea#description').characterCounter();
    });

    $scope.submitReport = function(){
        if($scope.reportData.description.length < 120){
            alert("The minimun caratheres are 120")
        }else{
            console.log( "great");
            $rootScope.reportData = $scope.reportData;
            console.log($rootScope.reportData);
            $location.path('/dragAnDrop');
        }
    };

}]);
