module.exports = function(db,mongoose) {

	var file = new mongoose.Schema({
		name : String,
		data : String,
		uploader : Schema.Types.ObjectId
	});
	file.methods.edit = function(_name, _data, _uploader){
		this.name = _name;
		this.data = _data;
		this.uploader = _uploader;
	};
	db.files = db.model('files', file);

};
