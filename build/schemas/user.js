module.exports = function(db,mongoose) {

	var user = new mongoose.Schema({
		name : String,
		email : String
	});
	user.methods.create = function(){
		this.name = "";
		this.email = "";
	};
	db.users = db.model('users', user);

};
